# Twitter API v2   

Documentation: https://developer.twitter.com/en/docs/platform-overview
Specification: https://api.twitter.com/2/openapi.json

## Prerequisites

+ Twitter Dev account
+ Bearer token

## Supported Operations

**0 out of 80 endpoints are failing.**

## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

